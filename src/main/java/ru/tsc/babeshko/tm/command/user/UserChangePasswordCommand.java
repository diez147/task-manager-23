package ru.tsc.babeshko.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tsc.babeshko.tm.enumerated.Role;
import ru.tsc.babeshko.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-change-password";

    @NotNull
    public static final String DESCRIPTION = "Change user password.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER NEW PASSWORD:]");
        @NotNull String password = TerminalUtil.nextLine();
        serviceLocator.getUserService().setPassword(userId, password);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}