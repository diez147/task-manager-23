package ru.tsc.babeshko.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tsc.babeshko.tm.enumerated.Role;
import ru.tsc.babeshko.tm.model.User;

public final class UserShowProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-show-profile";

    @NotNull
    public static final String DESCRIPTION = "Show user info.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull final User user = serviceLocator.getAuthService().getUser();
        showUser(user);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}