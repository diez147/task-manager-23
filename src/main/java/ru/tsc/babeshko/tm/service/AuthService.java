package ru.tsc.babeshko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.api.service.IAuthService;
import ru.tsc.babeshko.tm.api.service.IUserService;
import ru.tsc.babeshko.tm.enumerated.Role;
import ru.tsc.babeshko.tm.exception.field.EmptyLoginException;
import ru.tsc.babeshko.tm.exception.field.EmptyPasswordException;
import ru.tsc.babeshko.tm.exception.system.AccessDeniedException;
import ru.tsc.babeshko.tm.exception.system.IncorrectLoginOrPasswordException;
import ru.tsc.babeshko.tm.exception.system.LockedUserException;
import ru.tsc.babeshko.tm.exception.system.PermissionException;
import ru.tsc.babeshko.tm.model.User;
import ru.tsc.babeshko.tm.util.HashUtil;

import java.util.Arrays;

public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @Nullable
    private String userId;

    public AuthService(@NotNull final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void registry(@NotNull String login, @NotNull String password, @Nullable String email) {
        userService.create(login, password, email);
    }

    @Override
    public void login(@NotNull String login, @NotNull String password) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new IncorrectLoginOrPasswordException();
        if (user.getLocked()) throw new LockedUserException();
        @NotNull final String hash = HashUtil.salt(password);
        if (!hash.equals(user.getPasswordHash())) throw new IncorrectLoginOrPasswordException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @NotNull
    @Override
    public String getUserId() {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @NotNull
    @Override
    public User getUser() {
        if (!isAuth()) throw new AccessDeniedException();
        return userService.findOneById(userId);
    }

    @Override
    public void checkRoles(@Nullable final Role[] roles) {
        if (roles == null) return;
        @NotNull final User user = getUser();
        @NotNull final Role role = user.getRole();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }

}