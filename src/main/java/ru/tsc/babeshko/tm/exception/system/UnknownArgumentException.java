package ru.tsc.babeshko.tm.exception.system;

import ru.tsc.babeshko.tm.exception.AbstractException;

public final class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException() {
        super("Error! Argument not supported...");
    }

    public UnknownArgumentException(final String argument) {
        super("Error! Argument `" + argument + "` not supported...");
    }

}