package ru.tsc.babeshko.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.api.model.IWBS;
import ru.tsc.babeshko.tm.enumerated.Status;
import ru.tsc.babeshko.tm.util.DateUtil;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Project extends AbstractUserOwnedModel implements IWBS {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @NotNull
    private Date created = new Date();

    @Nullable
    private Date dateBegin;

    @Nullable
    private Date dateEnd;

    public Project(
            @NotNull final String name,
            @NotNull final Status status,
            @NotNull final Date dateBegin
    ) {
        this.name = name;
        this.status = status;
        this.dateBegin = dateBegin;
    }

    @Override
    public String toString() {
        return name + "; " + description + "; " + Status.toName(status) +
                "; Created - " + DateUtil.toString(created) +
                "; Begin - " + DateUtil.toString(dateBegin) +
                "; End - " + DateUtil.toString(dateEnd) + ";";
    }

}